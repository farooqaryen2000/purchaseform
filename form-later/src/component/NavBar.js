import React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import DynamicFormIcon from "@mui/icons-material/DynamicForm";
import { Link } from "react-router-dom";

export default function NavBar() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static" sx={{ background: "#37474f" }}>
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
          >
            <DynamicFormIcon />
          </IconButton>
          <Link to="/form/add/">
            <Button sx={{ color: "#fafafa" }}>Add Data</Button>
          </Link>
          <Link to="/">
            <Button sx={{ color: "#fafafa" }}>See data</Button>
          </Link>
        </Toolbar>
      </AppBar>
    </Box>
  );
}

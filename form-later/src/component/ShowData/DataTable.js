import React, { useEffect, useState } from "react";
import ShowData from "./showTableData";
import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@mui/material";
import axios from "axios";

function DataTable() {
  let [formData, setFormData] = useState([]);

  useEffect(() => {
    axios
      .get("http://localhost:5000/users")
      .then((res) => setFormData(res.data));
    console.log(formData);
  });

  // delete Date
  const onDelete = (id) => {
    if (window.confirm("are you sure to delete row?")) {
      axios
        .delete(`http://localhost:5000/users/${id}`)
        .then((res) => console.log(res.id));
    }
  };

  return (
    <TableContainer component={Paper}>
      <Table
        sx={{ minWidth: 650, mt: 2 }}
        size="small"
        aria-label="a dense table"
      >
        <TableHead sx={{ background: "black" }}>
          <TableRow>
            <TableCell sx={{ color: "#fff", fontSize: "15px" }}>ID</TableCell>
            <TableCell sx={{ color: "#fff", fontSize: "15px" }}>
              PurchaseCode
            </TableCell>
            <TableCell sx={{ color: "#fff", fontSize: "15px" }}>
              PurchaseName
            </TableCell>
            <TableCell sx={{ color: "#fff", fontSize: "15px" }}>
              PurchaseDate
            </TableCell>
            <TableCell sx={{ color: "#fff", fontSize: "15px" }}>
              PurchaseArea
            </TableCell>
            <TableCell sx={{ color: "#fff", fontSize: "15px" }}>
              ProductCategory
            </TableCell>
            <TableCell sx={{ color: "#fff", fontSize: "15px" }}>
              ProductName
            </TableCell>
            <TableCell sx={{ color: "#fff", fontSize: "15px" }}>
              ProductItem-ID
            </TableCell>
            <TableCell sx={{ color: "#fff", fontSize: "15px" }}>
              Action
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {formData.map((formdata) => (
            <TableRow align="left" key={formdata.id}>
              <ShowData data={formdata} onDelete={onDelete} />
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
export default DataTable;

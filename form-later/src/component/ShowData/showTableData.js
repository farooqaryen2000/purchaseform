import React from "react";
import TableCell from "@mui/material/TableCell";
import { IconButton } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";

export default function ShowData({ data, onDelete }) {
  return (
    <>
      <TableCell align="left">{data.id}</TableCell>
      <TableCell align="left">{data.formData.purchaseCode}</TableCell>
      <TableCell align="left">{data.formData.purchaseName}</TableCell>
      <TableCell align="left">{data.formData.purchaseDate}</TableCell>
      <TableCell align="left">{data.formData.purchaseArea}</TableCell>
      <TableCell align="left">{data.formData.productCategory}</TableCell>
      <TableCell align="left">{data.formData.productName}</TableCell>
      <TableCell align="left">{data.formData.productID}</TableCell>
      <TableCell align="left">
        <IconButton onClick={() => onDelete(data.id)}>
          <DeleteIcon color="error" />
        </IconButton>
      </TableCell>
    </>
  );
}

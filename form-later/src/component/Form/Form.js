import {
  Box,
  Button,
  Container,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Typography,
} from "@mui/material";
import axios from "axios";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

function AddData() {
  const [formData, setFormDate] = useState({
    purchaseCode: "",
    purchaseName: "",
    purchaseDate: "",
    purchaseArea: "",
    productCategory: "",
    productName: "",
    productID: "",
  });
  const navigate = useNavigate();

  // handle textfild values, set to fromData
  const handleTextfild = (e) => {
    setFormDate({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    axios
      .post("http://localhost:5000/users", { formData })
      .then((res) => console.log(formData));
    navigate("/");
  };
  //  array for category
  const Category = [
    "Mobile",
    "Game consoles",
    "Household furniture",
    "Home appliances",
    "Clothing",
    "Electronic",
  ];

  return (
    <Container component="main" maxWidth="md" sx={{ display: "block", mt: 1 }}>
      <Box
        component="form"
        onSubmit={handleSubmit}
        sx={{
          marginTop: 3,
          "& .MuiTextField-root": { m: 1, width: "45ch" },
        }}
      >
        <Typography component="h1" variant="h5">
          Fill The Blank
        </Typography>
        <TextField
          margin="normal"
          required
          fullWidth
          name="purchaseCode"
          label="purchaseCode"
          type="text"
          autoComplete="name"
          id="purchaseCode"
          onChange={handleTextfild}
        />
        <TextField
          margin="normal"
          required
          fullWidth
          name="purchaseName"
          label="Purchase Name"
          type="text"
          autoComplete="name"
          id="purchaseName"
          onChange={handleTextfild}
        />
        <TextField
          margin="normal"
          required
          fullWidth
          name="purchaseDate"
          type="Date"
          autoComplete="Date"
          id="purchaseDate"
          onChange={handleTextfild}
        />
        <TextField
          margin="normal"
          required
          fullWidth
          name="purchaseArea"
          label="Purchase Area"
          type="text"
          autoComplete="name"
          id="day"
          onChange={handleTextfild}
        />
        <FormControl
          sx={{ width: { xs: "45ch", md: "92ch" }, marginLeft: "8.3px" }}
        >
          <InputLabel id="demo-simple-select-label">
            Purchase Category
          </InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            name="productCategory"
            value={formData.productCategory}
            label="Purchase Category"
            onChange={handleTextfild}
            required
          >
            {Category.map((category) => (
              <MenuItem key={category.id} value={category}>
                {category}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <TextField
          margin="normal"
          required
          fullWidth
          name="productName"
          label="Product Name"
          type="text"
          autoComplete="name"
          id="productName"
          onChange={handleTextfild}
        />{" "}
        <TextField
          margin="normal"
          required
          fullWidth
          name="productID"
          label="Product Item ID"
          type="number"
          autoComplete="name"
          id="productID"
          onChange={handleTextfild}
        />
        <Button
          type="submit"
          variant="contained"
          sx={{ mt: 2, mb: 2, ml: 1, width: "45ch" }}
        >
          Submit
        </Button>
      </Box>
    </Container>
  );
}

export default AddData;
